package algotirmosestructuras;


public class AlgotirmosEstructuras {

    
    public static void main(String[] args){
         int indice = busqueda(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, 7);
        System.out.println("El elemento se encuentra en el índice: " + indice);
    }
    
    
    public static int busqueda(int[] arr, int elementoBuscado) {
        int izquierda = 0, derecha = arr.length - 1;
        while (izquierda <= derecha) {
            int medio = (izquierda + derecha) / 2;
            int medioValor = arr[medio];

            if (medioValor == elementoBuscado) {
                return medio;
            } else if (elementoBuscado < medioValor) {
                derecha = medio - 1;
            } else {
                izquierda = medio + 1;
            }
        }
        return -1;
    }
        
  
   
    
}

